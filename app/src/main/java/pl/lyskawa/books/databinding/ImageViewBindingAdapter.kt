package pl.lyskawa.books.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("netSrc")
fun bindNetSrc(view: ImageView, url: String?) {
    url?.let {
        Glide.with(view)
            .load(it)
            .into(view)
    }
}