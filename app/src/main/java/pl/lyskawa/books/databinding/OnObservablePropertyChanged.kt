package pl.lyskawa.books.databinding

import androidx.databinding.Observable


fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit): Observable.OnPropertyChangedCallback{
    val listener = OnObservablePropertyChanged { sender, _ -> sender?.let { callback(it as T) } }
    this.addOnPropertyChangedCallback(listener)
    return listener
}

private class OnObservablePropertyChanged(
    val listener: (sender: Observable?, propertyChanged: Int) -> Unit
) : Observable.OnPropertyChangedCallback() {

    override fun onPropertyChanged(sender: Observable?, propertyId: Int) =
        listener(sender, propertyId)
}
