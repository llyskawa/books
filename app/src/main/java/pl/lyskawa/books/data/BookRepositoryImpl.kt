package pl.lyskawa.books.data

import io.reactivex.Completable
import io.reactivex.Single
import pl.lyskawa.books.data.network.BookService
import pl.lyskawa.books.domain.BookEntity
import pl.lyskawa.books.domain.BookRepository

class BookRepositoryImpl(val dao: BookDao, val service: BookService) : BookRepository {
    override fun loadAllBooks(): Single<List<BookEntity>> =
        dao.loadAllBooks().map { list -> list.map { dbModel -> dbModel.toEntity() } }


    override fun loadBooks(filter: String?) =
        dao.loadBooks(filter)
            .map { list -> list.map { dbModel -> dbModel.toEntity() } }


    override fun refresh(): Completable =
        service.fetchBooks()
            .flatMapCompletable { response ->
                dao.delete()
                dao.insert(response.map { it.toDBEntity() })
            }

    override fun findBook(bookId: Long): Single<BookEntity> =
        dao.findBookById(bookId)
            .map { it.toEntity() }
}