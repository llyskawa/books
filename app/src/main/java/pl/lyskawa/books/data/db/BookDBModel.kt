package pl.lyskawa.books.data.db

import pl.lyskawa.books.domain.BookEntity

data class BookDBModel(
    val id: Long,
    val title: String,
    val author: String,
    val imageLink: String
) {
    fun toEntity(): BookEntity = BookEntity(id, title, author, imageLink)
}