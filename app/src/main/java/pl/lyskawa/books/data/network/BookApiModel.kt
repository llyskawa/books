package pl.lyskawa.books.data.network

import pl.lyskawa.books.data.db.BookDBEntity

data class BookApiModel(
    val author: String,
    val country: String,
    val imageLink: String,
    val language: String,
    val link: String,
    val pages: Int,
    val title: String,
    val year: Int
) {
    fun toDBEntity(): BookDBEntity =
        BookDBEntity(null, author, country, imageLink, language, link, pages, title, year)
}