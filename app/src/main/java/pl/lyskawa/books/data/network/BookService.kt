package pl.lyskawa.books.data.network

import io.reactivex.Single
import retrofit2.http.GET

interface BookService{
    @GET("books.json")
    fun fetchBooks(): Single<List<BookApiModel>>
}