package pl.lyskawa.books.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import pl.lyskawa.books.data.db.BookDBEntity
import pl.lyskawa.books.data.db.BookDBModel

@Dao
interface BookDao {

    @Insert
    fun insert(books: List<BookDBEntity>): Completable

    @Query("""DELETE FROM books""")
    fun delete()

    @Query("""SELECT id, title, author, imageLink FROM books""")
    fun loadAllBooks(): Single<List<BookDBModel>>

    @Query("""SELECT id, title, author, imageLink FROM books WHERE title LIKE :filter || '%'""")
    fun loadBooks(filter: String?): Single<List<BookDBModel>>

    @Query("""SELECT id, title, author, imageLink FROM books WHERE id=:bookId""")
    fun findBookById(bookId: Long): Single<BookDBModel>
}