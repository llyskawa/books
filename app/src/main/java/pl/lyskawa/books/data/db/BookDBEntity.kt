package pl.lyskawa.books.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "books")
data class BookDBEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long?=null,

    val author:String,
    val country:String,
    val imageLink:String,
    val language: String,
    val link: String,
    val pages: Int,
    val title: String,
    val year: Int
)