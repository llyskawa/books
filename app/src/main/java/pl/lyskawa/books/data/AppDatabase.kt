package pl.lyskawa.books.data

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.lyskawa.books.data.db.BookDBEntity

@Database(entities = [BookDBEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDao
}