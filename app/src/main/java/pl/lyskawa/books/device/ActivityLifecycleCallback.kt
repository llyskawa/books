package pl.lyskawa.books.device

import android.app.Activity
import android.app.Application
import android.os.Bundle

typealias LifecycleCallback = (Activity) -> Unit

class ActivityLifecycleCallback(
    val onCreated: LifecycleCallback? = null,
    val onCreatedBundle: ((Activity, Bundle?) -> Unit)? = null,
    val onStarted: LifecycleCallback? = null,
    val onResumed: LifecycleCallback? = null,
    val onSaveInstanceState: ((Activity, Bundle?) -> Unit)? = null,
    val onPaused: LifecycleCallback? = null,
    val onStopped: LifecycleCallback? = null,
    val onDestroyed: LifecycleCallback? = null
) : Application.ActivityLifecycleCallbacks {

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        onCreatedBundle?.invoke(activity, savedInstanceState)
        onCreated?.invoke(activity)
    }

    override fun onActivityStarted(activity: Activity) {
        onStarted?.invoke(activity)
    }

    override fun onActivityResumed(activity: Activity) {
        onResumed?.invoke(activity)
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {
        onSaveInstanceState?.invoke(activity, outState)
    }

    override fun onActivityPaused(activity: Activity) {
        onPaused?.invoke(activity)
    }

    override fun onActivityStopped(activity: Activity) {
        onStopped?.invoke(activity)
    }

    override fun onActivityDestroyed(activity: Activity) {
        onDestroyed?.invoke(activity)
    }
}