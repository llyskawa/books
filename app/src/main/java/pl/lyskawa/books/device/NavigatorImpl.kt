package pl.lyskawa.books.device

import android.app.Activity
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import pl.lyskawa.books.R
import pl.lyskawa.books.domain.Navigator
import java.lang.ref.WeakReference

class NavigatorImpl : Navigator {

    private var currentActivity = WeakReference<Activity>(null)


    override fun registerActivity(activity: Activity) {
        currentActivity = WeakReference(activity)
    }

    override fun navigateByAction(action: NavDirections) {
        synchronized(this) {
            currentActivity.navController()?.navigate(action)
        }
    }

    override fun navigateUp() {
        synchronized(this) {
            currentActivity.navController()?.navigateUp()
        }
    }


    private fun WeakReference<Activity>.navController(): NavController? =
        get()?.let {
            Navigation.findNavController(it, R.id.container)
        }


}