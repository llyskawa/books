package pl.lyskawa.books.di

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.module
import pl.lyskawa.books.BuildConfig
import pl.lyskawa.books.data.network.BookService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { GsonBuilder().serializeNulls().create() }

    single {
        OkHttpClient.Builder()
            .addInterceptor {
                val request = it.request()
                Log.d("HTTP", "[${request.method()}] ${request.url()}")
                it.proceed(it.request())
            }
            .build()
    }

    single<BookService> {
        Retrofit.Builder()
            .client(get())
            .baseUrl(BuildConfig.BACKEND_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(BookService::class.java)
    }
}