package pl.lyskawa.books.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.lyskawa.books.device.NavigatorImpl
import pl.lyskawa.books.domain.Navigator
import pl.lyskawa.books.ui.book.BookViewModel
import pl.lyskawa.books.ui.library.LibraryViewModel

val appModule = module {
    viewModel { LibraryViewModel(get()) }
    viewModel { BookViewModel(get()) }

    single<Navigator> { NavigatorImpl() }
}