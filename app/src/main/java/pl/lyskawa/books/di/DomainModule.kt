package pl.lyskawa.books.di

import org.koin.dsl.module
import pl.lyskawa.books.domain.book.BookInteractor
import pl.lyskawa.books.domain.library.LibraryInteractor

val domainModule = module {
    single { LibraryInteractor(get(), get()) }
    single { BookInteractor(get(), get()) }
}