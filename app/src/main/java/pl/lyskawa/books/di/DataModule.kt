package pl.lyskawa.books.di

import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import pl.lyskawa.books.data.AppDatabase
import pl.lyskawa.books.data.BookRepositoryImpl
import pl.lyskawa.books.domain.BookRepository

val dataModule = module {
    factory<BookRepository> { BookRepositoryImpl(get(), get()) }

    single { Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "books-db").build() }

    factory{ get<AppDatabase>().bookDao() }
}