package pl.lyskawa.books.ui.library.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.books.domain.BookEntity

class LibraryRecyclerView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, style: Int = 0) : RecyclerView(context, attrs, style) {

    val adapter = LibraryAdapter()

    init {
        setAdapter(adapter)
        addItemDecoration(DividerItemDecoration(context, VERTICAL))
    }

    fun setItems(books: List<BookEntity>) {
        adapter.books = books
    }

    fun setOnItemClick(callback: (BookEntity) -> Unit) {
        adapter.onItemClick = callback
    }
}

