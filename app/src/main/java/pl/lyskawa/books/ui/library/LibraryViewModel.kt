package pl.lyskawa.books.ui.library

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.lyskawa.books.databinding.addOnPropertyChanged
import pl.lyskawa.books.domain.BookEntity
import pl.lyskawa.books.domain.library.LibraryInteractor
import java.util.concurrent.TimeUnit

class LibraryViewModel(val interactor: LibraryInteractor) : ViewModel() {

    val books: ObservableList<BookEntity> = ObservableArrayList()
    val onBookClick: (BookEntity) -> Unit = interactor::showDetails
    val isRefreshing = ObservableBoolean()
    val filter = ObservableField<String?>()

    private val filterSubject = BehaviorSubject.create<String>()
    private val filterListener =
        filter.addOnPropertyChanged { property ->
            val value = property.get()
            if (value.isNullOrEmpty()) interactor.loadBooks().addTo(compositeDisposable)
            else filterSubject.onNext(value)
        }

    private val compositeDisposable = CompositeDisposable()

    init {
        refresh()
        interactor.books()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { books ->
                this.books.clear()
                this.books.addAll(books)
            }
            .addTo(compositeDisposable)

        filterSubject.debounce(500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .subscribe { interactor.filterQuery(it) }
            .addTo(compositeDisposable)

        interactor.loadBooks().addTo(compositeDisposable)
    }

    fun refresh() {
        isRefreshing.set(true)
        interactor.refresh()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe {
                isRefreshing.set(false)
                if (filterSubject.hasValue()) {
                    interactor.filterQuery(filterSubject.value)
                } else {
                    interactor.loadBooks()
                }
            }
            .addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        filter.removeOnPropertyChangedCallback(filterListener)
    }
}