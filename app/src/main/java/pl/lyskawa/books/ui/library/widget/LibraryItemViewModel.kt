package pl.lyskawa.books.ui.library.widget

import androidx.databinding.ObservableField
import pl.lyskawa.books.domain.BookEntity

class LibraryItemViewModel(val onItemClick: (BookEntity) -> Unit) {
    val name = ObservableField<String>("")
    private lateinit var entity: BookEntity

    fun bind(entity: BookEntity) {
        this.entity = entity
        name.set(entity.name)
    }

    fun onClick() {
        onItemClick(entity)
    }

}