package pl.lyskawa.books.ui.library.widget

import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.books.databinding.ItemLibraryBinding
import pl.lyskawa.books.domain.BookEntity

class LibraryItemViewHolder(private val binding: ItemLibraryBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(entity: BookEntity) {
        binding.viewModel?.bind(entity)
        binding.executePendingBindings()
    }
}