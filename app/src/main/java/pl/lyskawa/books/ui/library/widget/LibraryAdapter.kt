package pl.lyskawa.books.ui.library.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.books.databinding.ItemLibraryBinding
import pl.lyskawa.books.domain.BookEntity

class LibraryAdapter : RecyclerView.Adapter<LibraryItemViewHolder>() {

    var books: List<BookEntity>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var onItemClick: (BookEntity) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemLibraryBinding.inflate(inflater, parent, false)
        binding.viewModel = LibraryItemViewModel(onItemClick)
        return LibraryItemViewHolder(binding)
    }

    override fun getItemCount(): Int =
        books?.size ?: 0

    override fun onBindViewHolder(holder: LibraryItemViewHolder, position: Int) =
        holder.bind(books!![position])
}