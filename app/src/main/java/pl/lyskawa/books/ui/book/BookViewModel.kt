package pl.lyskawa.books.ui.book

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import pl.lyskawa.books.BuildConfig
import pl.lyskawa.books.domain.BookEntity
import pl.lyskawa.books.domain.book.BookInteractor

class BookViewModel(private val interactor: BookInteractor) : ViewModel() {

    val title = ObservableField<String>()
    val url = ObservableField<String>()
    val author = ObservableField<String>()

    private val compositeDisposable = CompositeDisposable()

    fun loadBook(bookId: Long) {
        interactor.loadBook(bookId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy {
                title.set(it.name)
                url.set(it.fullImageUrl())
                author.set(it.author)
            }
            .addTo(compositeDisposable)
    }

    fun BookEntity.fullImageUrl() = "${BuildConfig.BACKEND_URL}/$imageUrl"

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}