package pl.lyskawa.books

import android.app.Application
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import pl.lyskawa.books.device.ActivityLifecycleCallback
import pl.lyskawa.books.di.appModule
import pl.lyskawa.books.di.dataModule
import pl.lyskawa.books.di.domainModule
import pl.lyskawa.books.di.networkModule
import pl.lyskawa.books.domain.Navigator

class App : Application() {

    private val navigator: Navigator by inject()

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule, dataModule, domainModule, networkModule)
        }
        registerActivityLifecycleCallbacks(
            ActivityLifecycleCallback(
                onStarted = navigator::registerActivity
            )
        )
    }
}