package pl.lyskawa.books.domain.book

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import pl.lyskawa.books.domain.BookEntity
import pl.lyskawa.books.domain.BookRepository
import pl.lyskawa.books.domain.Navigator

class BookInteractor(
    private val repository: BookRepository,
    private val navigator: Navigator
) {
    fun loadBook(bookId: Long): Single<BookEntity> =
        repository.findBook(bookId)
            .subscribeOn(Schedulers.io())

    fun goBack() =
        navigator.navigateUp()
}