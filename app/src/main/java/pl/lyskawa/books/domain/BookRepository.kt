package pl.lyskawa.books.domain

import io.reactivex.Completable
import io.reactivex.Single

interface BookRepository {
    fun refresh(): Completable
    fun loadAllBooks(): Single<List<BookEntity>>
    fun loadBooks(filter: String? = null): Single<List<BookEntity>>
    fun findBook(bookId: Long): Single<BookEntity>
}