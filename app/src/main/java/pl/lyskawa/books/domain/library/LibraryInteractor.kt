package pl.lyskawa.books.domain.library

import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.lyskawa.books.domain.BookEntity
import pl.lyskawa.books.domain.BookRepository
import pl.lyskawa.books.domain.Navigator
import pl.lyskawa.books.ui.library.LibraryFragmentDirections

class LibraryInteractor(
    private val repository: BookRepository,
    private val navigator: Navigator
) {

    private val books = BehaviorSubject.create<List<BookEntity>>()

    fun books(): Flowable<List<BookEntity>> = books.toFlowable(BackpressureStrategy.LATEST)

    fun loadBooks() =
        repository.loadAllBooks().subscribeOn(Schedulers.io()).subscribeBy { books.onNext(it) }

    fun filterQuery(filter: String?) =
        repository.loadBooks(filter).subscribeOn(Schedulers.io()).subscribeBy { books.onNext(it) }
    
    fun refresh(): Completable =
        repository.refresh().subscribeOn(Schedulers.io())

    fun showDetails(entity: BookEntity) {
        navigator.navigateByAction(
            LibraryFragmentDirections
                .actionLibraryFragmentToBookFragment(entity.id)
        )
    }
}