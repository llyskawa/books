package pl.lyskawa.books.domain

data class BookEntity(val id:Long, val name:String, val author: String, val imageUrl:String)