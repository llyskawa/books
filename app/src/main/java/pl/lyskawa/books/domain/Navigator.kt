package pl.lyskawa.books.domain

import android.app.Activity
import androidx.navigation.NavDirections

interface Navigator {
    fun registerActivity(activity: Activity)
    fun navigateByAction(action: NavDirections)
    fun navigateUp()
}